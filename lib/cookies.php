<?php



// Setting the master cookie cookie
// time() + xxxx -> currently set for 690 days which is 23 months
if (isset($_POST['consent-given'])){
    setcookie("master", "accepted", time() + 60*60*24*690, "/");
    return true;
}

// Removing All cookies - user withdrawed the consent
if (isset($_POST['consent-withdrawn'])){

    if (isset($_SERVER['HTTP_COOKIE'])) {
        $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
        foreach($cookies as $cookie) {
            $parts = explode('=', $cookie);
            $name = trim($parts[0]);
            setcookie($name, '', time()-1000);
            setcookie($name, '', time()-1000, '/');
        }
    }

}

?>