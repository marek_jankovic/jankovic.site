
// ***LANDING PAGE*** main nav menu
// main nav is not shown on first <section>
// getting the viewporth width
html_width = $("html").width();

// getting current position of first section from the top
if ($("#aboutMe") !== 'undefined'){
	showNav = $("#aboutMe").offset().top;
	showNav -= 40;
}


// BACK TO TOP button hide/show

$(document).ready(function(){
	$("#back-to-top").hide();
});

// On scroll - check the position and either show or hide
$(window).scroll(function(){
	if(window.pageYOffset < showNav){
		$("#back-to-top").hide();
	}else{
		$("#back-to-top").show();
	}
});	

// if desktop (more than 992px) then use dynamic show/hide
//  based on the positiong
if(html_width > 992) {
	
	// menu hidden by default
	$(document).ready(function(){
		$("nav").hide();
	});
	
	// On scroll - check the position and either show or hide
	$(window).scroll(function(){
		if(window.pageYOffset < showNav){
			$("nav").hide();
		}else{
			$("nav").show();
		}
	});	
}

// ***LANDING PAGE*** button Interaction
var aboutMe = $(".about-btn");

aboutMe.on("click", function(){
	$("nav").show();
});

aboutMe.on("mouseenter", function(){
  $(this).find($("svg")).attr("data-fa-transform", "rotate-90");
});

aboutMe.on("mouseleave", function(){
  $(this).find($("svg")).removeAttr("data-fa-transform");
});


// AJAX SCRIPT
$(document).ready(function(){
	
	// Contact form handling
	$("#main_contactForm").on('submit', function(e){

		// Stops the form from submitting itself to the server
		e.preventDefault();

		var data = $('#main_contactForm').serializeArray();

		$.ajax({			
			type: "POST",
			url: "lib/jankovic_Controller.php",
			data: data,
			success: function(data){
				console.log(data);
				$('#formSubmitButton').hide();
				$('#formSentMessage').show();
				// $('#main_contactForm').each(function(){
				// 	this.reset();
				// });
			}
		});
	});

	// Cookie consent handling
	// Hiding the cookie info if the master cookie has been set
	if($.cookie("master") !== 'undefined' && $.cookie("master") == 'accepted'){
		$('#cookie-consent').hide();
	} else{
		// Height of the cookie div
		var elementHeight = $('#cookie-consent').outerHeight();
		var windowHeight = $(window).outerHeight();

		$('#cookie-consent').css("top", windowHeight - (elementHeight));
	}	

	// AJAX call for setting the master cookie
	$('#cookie-btn').on('click', function(){
		$.ajax({
			type: "POST",
			url: "lib/cookies.php",
			data: "consent-given",
			success: function(data){
				console.log(data);
				$('#cookie-consent').addClass("animated fadeOutDown");
			}
		});
	});

});

// *** CAROUSEL (portfolio) script
$('.main-carousel').flickity({
	// options
	cellAlign: 'center',
	contain: true,
	// wrapAround: true,
	initialIndex: 0,
	// freeScroll: true
  });


//  Getting href values of hidden a tag from html 


$(document).ready(function(){
	var bcg = $('.is-selected .bcg-img').attr("href");
	var car_name = $('.is-selected .carousel-name').text();
	var car_desc = $('.is-selected .carousel-description').text()
	var car_link = $('.is-selected .portfolio-link').attr("href");

	$('#portfolio').css('background', "url(" + bcg + ")" );
	$('#carousel-portfolio').text(car_name);
	$('#carousel-long-desc').text(car_desc);
	$('#project-button').prop("href", car_link);
});

  //  Getting name value & long desc. value for changing h4 in portfolio section
 //   Getting href value for the button link
$('.main-carousel').on( 'select.flickity', function() {
	var bcg = $('.is-selected .bcg-img').attr("href");
	var car_name = $('.is-selected .carousel-name').text();
	var car_desc = $('.is-selected .carousel-description').text()
	var car_link = $('.is-selected .portfolio-link').attr("href");

	$('#portfolio').css('background', "url(" + bcg + ")" );
	$('#carousel-portfolio').text(car_name);
	$('#carousel-long-desc').text(car_desc);
	$('#project-button').attr("href", car_link);

  });