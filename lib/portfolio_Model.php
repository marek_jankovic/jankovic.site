<?php

class portfolioModel{

    public $id;
    public $name;
    public $type;
    public $description;
    public $thumbnail;
    public $background;
    public $link;

    function getAllItems(){
        $sql = "SELECT * FROM portfolio_list ORDER BY id ASC";

        $result = Config::$db->query($sql);

        // Array of Objects, which represents individual portfolio items
        $r = [];

        if($result->num_rows > 0){

            while($row = $result->fetch_assoc()){
                $item = new portfolioModel();

                $item->id = $row['id'];
                $item->name = $row['name'];
                $item->type = $row['type'];
                $item->description = $row['description'];
                $item->thumbnail = $row['thumbnail'];
                $item->background = $row['background'];
                $item->link = $row['link'];

                $r[] = $item;
            }

        }

        return $r;
    }

}


?>