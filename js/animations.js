// Custom function for determining whether the element is in the viewport or not

// Offset function is used to find the position 
// relative to parent element(body in this case)
// and top refers to the distance from the top.

// OuterHeight allows us to find the height of
// element including border, padding and optionally padding.
// So starting point of element + height = element bottom

// ScrollTop returns the relative position from the scrollbar
// position to object matched, which in this case is our window.

// Same as finding the bottom of the element
$.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
  
    return elementBottom > viewportTop && elementTop < viewportBottom;
};

// Sections variables
var home = $('#home');
var about = $('#aboutMe');
var portfolio = $('#portfolio');
var contact = $('#contact');

// Landing page is animated once the document is ready, other sectio only
// when visible and some action (scroll, resize happend)
$(document).ready(function(){
    
    if (home.isInViewport()){
        $('#myName').addClass("animated fadeIn slower");
    }

    // Event will fire on both actions - resize AND/OR scroll
    $(window).on('resize scroll', function() {
    
    // Checking if Section is visible with custom Function

        // ABOUT
        if (about.isInViewport()){
            $('#profile_img').addClass("animated slideInLeft");
            $('#about-intro-txt').addClass("animated slideInRight");
            $('#about-code-txt').addClass("animated slideInRight delay-025s");
            $('#about-study-txt').addClass("animated slideInRight delay-04s");
        }


        // PORTFOLIO
        if (portfolio.isInViewport()){

            if($('#portfolio-carousel').isInViewport()){
                $('#portfolio-carousel').addClass("animated slideInUp slow");
            }

            $('#portfolio_main_description').addClass("animated slideInLeft");
        }

        // CONTACT
        if (contact.isInViewport()){
            $('#main_contactForm').addClass("animated slideInLeft");
            $('#contact-social').addClass("animated slideInRight delay-025s");
        }

    });


});


