<!doctype html>
<html lang="en">

<head>

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-KZ6BLK8');</script>
  <!-- End Google Tag Manager -->

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <!-- Font Awesome CDN -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>


  <!-- Flickity (carousel) CSS -->
  <link rel="stylesheet" type="text/css" href="css/flickity.min.css">

  <!-- Custom stylesheet -->
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <!-- Animate CSS -->
  <link rel="stylesheet" type="text/css" href="css/animate.css">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="57x57" href="assets/favico/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="assets/favico/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="assets/favico/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/favico/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="assets/favico/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="assets/favico/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="assets/favico/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="assets/favico/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="assets/favico/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="assets/favico/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/favico/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="assets/favico/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/favico/favicon-16x16.png">
  <link rel="manifest" href="assets/favico/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="assets/favico/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <!-- PAPER JS -->
  <script type="text/javascript" src="lib/paper-full.js"></script>
  <script type="text/paperscript" canvas="myCanvas">

    var mousePos = view.center + [view.bounds.width / 5, 100];
        var position = view.center;

        function onFrame(event) {
          position += (mousePos - position) / 10;
          var vector = (view.center - position) / 20;
          moveStars(vector);
        }

        function onMouseMove(event) {
          mousePos = event.point;
        }

        function onKeyDown(event) {
          if (event.key == 'space')
            project.activeLayer.selected = !project.activeLayer.selected;
        }

        var moveStars = new function() {
          // The amount of symbol we want to place;
          var count = 150;

          // Create a symbol, which we will use to place instances of later:
     
          

          // Place the instances of the symbol:
          for (var i = 0; i < count; i++) {

            var path = new Path.Circle({
            center: [0, 0],
            radius: 5,
            fillColor: '#'+(Math.random()*0xffffff<<0).toString(16),

          });

            var symbol = new Symbol(path);

            // The center position is a random point in the view:
            var center = Point.random() * view.size;
            var placed = symbol.place(center);
            placed.scale(i / count + 0.01);
            placed.data = {
              vector: new Point({
                angle: Math.random() * 360,
                length : (i / count) * Math.random() / 5
              })
            };
          }

          var vector = new Point({
            angle: 45,
            length: 0
          });

          function keepInView(item) {
            var position = item.position;
            var viewBounds = view.bounds;
            if (position.isInside(viewBounds))
              return;
            var itemBounds = item.bounds;
            if (position.x > viewBounds.width + 5) {
              position.x = -item.bounds.width;
            }

            if (position.x < -itemBounds.width - 5) {
              position.x = viewBounds.width;
            }

            if (position.y > viewBounds.height + 5) {
              position.y = -itemBounds.height;
            }

            if (position.y < -itemBounds.height - 5) {
              position.y = viewBounds.height
            }
          }

          return function(vector) {
            // Run through the active layer's children list and change
            // the position of the placed symbols:
            var layer = project.activeLayer;
            for (var i = 0; i < count; i++) {
              var item = layer.children[i];
              var size = item.bounds.size;
              var length = vector.length / 10 * size.width / 10;
              item.position += vector.normalize(length) + item.data.vector;
              keepInView(item);
            }
          };
        };



	
    </script>



  <title>Marek Jankovic | Privacy Policy</title>
</head>

<body>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KZ6BLK8"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <!-- PHP Requirements -->
  <?php
    try {

      require_once "lib/jankovic_Controller.php";
      require_once "lib/jankovic_Model.php";
      require_once "lib/portfolio_Controller.php";
      require_once "lib/portfolio_Model.php";
      require_once "lib/cookies.php";
    } catch (Exception $e) {
      echo 'Caught exception: ' . $e->getMessage() . "\n";
    }

    // VERIFYING whether the master cookie has been already set
    // Such a cookie is saved only if user accepted the consent before
    // We don't want to show the consent info again if the master cookie is set
    
    //  TO DO

  ?>

  <!-- COOKIE consent -->
  <div class="justify-content bd-highlight mb-3" id="cookie-consent">
    <p>
      Hey there! Let's make it short - I use cookies, just to keep track visits and basic analytic data. 
       Why, you may ask - simply for constant improving and keeping this website on the right track. 
       <strong>I don't store nor use any of your personal data.</strong>
       <br>
       <br>
       Find out more.
    </p>
    <div class="col-md d-flex align-items-end flex-column" id="cookie-accept">
      <a id="cookie-btn" class="btn btn-danger">Got it!</a>
    </div>
  </div>

  <!-- GLOBAL NAVIGATION menu -->
  <nav class="navbar sticky-top navbar-dark bg-dark align-content-end navbar-expand-lg">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsedMainNav">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsedMainNav">
      <!-- Navbar content -->
      <a class="navbar-brand animatedSpan" href="index.php#home">〉MJ</a>
      <a class="nav-item nav-link" href="index.php#home">Home</a>
      <a class="nav-item nav-link" href="index.php#aboutMe">About Me</a>
      <a class="nav-item nav-link" href="index.php#portfolio">Portfolio</a>
      <a class="btn btn-success nav-item nav-link" href="index.php#contact-bottom">Contact</a>
    </div>
  </nav>

  <!-- LANDING PAGE INTRO-->
  <!-- Each full screen wrapped by section -->
  <section class="landing" id="home">
    <a id="top"></a>
    <div id="overlayDiv">
      <!-- overlay Div used only to show content above canvas element -->
      <div class="container">
        <div class="col-md-12" id="privacy-policy" >
            <h3>
                What are cookies?
            </h3>
            <p>
                Cookies are small units of data temporarily stored on the hard disk of your computer by your browser
                 that are necessary for using our website. <strong> do not contain any personal information</strong> about you 
                 and cannot be used to identify an individual user. A cookie often includes a unique identifier, which
                  is an anonymous number (randomly generated) and is stored on your device. Some expire at the end of your
                   website session; others remain on your computer for longer.
            </p>

            <h3>
                Cookies used on this website
            </h3>
            <ul>
                <li>
                    <h4>
                        Master Cookie
                    </h4>
                    <p>
                    This Cookie is used by this site to remember a user's choice about cookies on the website.
                    </p>
                </li>
                <li>
                    <h4>
                        Google Analytics & Tagmanager
                    </h4>
                    <p>
                        These 3rd party cookies are used for anonymous tracking of user visits & other basic metrics. I use these data for improving my website and your experience.
                    </p>
                </li>
            </ul>

            <p>
                Feel free to visit www.aboutcookies.org, which contains pretty comprehensive info on how to work with cookiec - how different browsers use them and how to change the settings.
            </p>

            <p>
                You may click below if you want to get rid of the cookies used by this domain.
            </p>
            
            <button type="button" class="btn btn-light" id="cookie-withdraw">Remove cookies</button>


        </div>
      </div>
    </div>
    </div>
    <canvas id="myCanvas" resize></canvas>
  </section>



  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script type="text/javascript" src="lib/jquery-3.3.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" src="lib/jquery.cookie.js"></script>
  <script type="text/javascript" src="js/privacy-policy.js"></script>
  <!-- <script type="text/javascript" src="js/anime.min.js"></script> -->
  <script type="text/javascript" src="js/animations.js"></script>


</body>

</html>