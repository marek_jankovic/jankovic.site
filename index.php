<!doctype html>
<html lang="en">

<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KZ6BLK8');</script>
<!-- End Google Tag Manager -->

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <!-- Font Awesome CDN -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>


  <!-- Flickity (carousel) CSS -->
  <link rel="stylesheet" type="text/css" href="css/flickity.min.css">

  <!-- Custom stylesheet -->
  <link rel="stylesheet" type="text/css" href="css/style.min.css">

  <!-- Animate CSS -->
  <link rel="stylesheet" type="text/css" href="css/animate.min.css">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="57x57" href="assets/favico/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="assets/favico/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="assets/favico/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/favico/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="assets/favico/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="assets/favico/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="assets/favico/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="assets/favico/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="assets/favico/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="assets/favico/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/favico/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="assets/favico/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/favico/favicon-16x16.png">
  <link rel="manifest" href="assets/favico/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="assets/favico/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <!-- META TAGS -->
  <meta name="description" content="Marek Jankovic - an aspiring full-stack web developer. Check out my website & portfolio. Get inspired by design or consider collaboration and contact me!">
  <meta property="og:title" content="Marek Jankovic - Aspiring Full Stack web developer">
  <meta property="og:site_name" content="">
  <meta property="og:url" content="jankovic.site">
  <meta property="og:description" content="Interested in web-dev, coding & my learning journey? Do you like PHP & Laravel? Get inspired by design, check my portfolio and consider collaboration - open to any ideas! ">
  <meta property="og:type" content="website">
  <meta property="og:image" content="https://jankovic.site/assets/paralax-picture-bw.png">

  <!-- PAPER JS -->
  <script type="text/javascript" src="lib/paper-full.js"></script>
  <script type="text/paperscript" canvas="myCanvas">

    var mousePos = view.center + [view.bounds.width / 5, 100];
        var position = view.center;

        function onFrame(event) {
          position += (mousePos - position) / 10;
          var vector = (view.center - position) / 20;
          moveStars(vector);
        }

        function onMouseMove(event) {
          mousePos = event.point;
        }

        function onKeyDown(event) {
          if (event.key == 'space')
            project.activeLayer.selected = !project.activeLayer.selected;
        }

        var moveStars = new function() {
          // The amount of symbol we want to place;
          var count = 150;

          // Create a symbol, which we will use to place instances of later:



          // Place the instances of the symbol:
          for (var i = 0; i < count; i++) {

            var path = new Path.Circle({
            center: [0, 0],
            radius: 5,
            fillColor: '#'+(Math.random()*0xffffff<<0).toString(16),

          });

            var symbol = new Symbol(path);

            // The center position is a random point in the view:
            var center = Point.random() * view.size;
            var placed = symbol.place(center);
            placed.scale(i / count + 0.01);
            placed.data = {
              vector: new Point({
                angle: Math.random() * 360,
                length : (i / count) * Math.random() / 5
              })
            };
          }

          var vector = new Point({
            angle: 45,
            length: 0
          });

          function keepInView(item) {
            var position = item.position;
            var viewBounds = view.bounds;
            if (position.isInside(viewBounds))
              return;
            var itemBounds = item.bounds;
            if (position.x > viewBounds.width + 5) {
              position.x = -item.bounds.width;
            }

            if (position.x < -itemBounds.width - 5) {
              position.x = viewBounds.width;
            }

            if (position.y > viewBounds.height + 5) {
              position.y = -itemBounds.height;
            }

            if (position.y < -itemBounds.height - 5) {
              position.y = viewBounds.height
            }
          }

          return function(vector) {
            // Run through the active layer's children list and change
            // the position of the placed symbols:
            var layer = project.activeLayer;
            for (var i = 0; i < count; i++) {
              var item = layer.children[i];
              var size = item.bounds.size;
              var length = vector.length / 10 * size.width / 10;
              item.position += vector.normalize(length) + item.data.vector;
              keepInView(item);
            }
          };
        };




    </script>



  <title>Marek Jankovic | Web Developer</title>
</head>

<body>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KZ6BLK8"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <!-- PHP Requirements -->
  <?php
try {

    require_once "lib/jankovic_Controller.php";
    require_once "lib/jankovic_Model.php";
    require_once "lib/portfolio_Controller.php";
    require_once "lib/portfolio_Model.php";
    require_once "lib/cookies.php";
    } catch (Exception $e) {
    echo 'Caught exception: ' . $e->getMessage() . "\n";
    }
  ?>

  <!-- COOKIE consent -->
  <div class="justify-content bd-highlight mb-3" id="cookie-consent">
    <p>
      Hey there! Let's make it short - I use cookies, just to keep track visits and basic analytic data.
       Why, you may ask - simply for constant improving and keeping this website on the right track.
       <strong>I don't store nor use any of your personal data.</strong>
       <br>
       <br>
       <a href="privacy-policy.php">Find out more.</a>
    </p>
    <div class="col-md d-flex align-items-end flex-column" id="cookie-accept">
      <a id="cookie-btn" class="btn btn-danger">Got it!</a>
    </div>
  </div>

  <!-- GLOBAL NAVIGATION menu -->
  <nav class="navbar sticky-top navbar-dark bg-dark align-content-end navbar-expand-lg">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsedMainNav">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsedMainNav">
      <!-- Navbar content -->
      <a class="navbar-brand animatedSpan" href="#home">〉MJ</a>
      <a class="nav-item nav-link" href="#home">Home</a>
      <a class="nav-item nav-link" href="#aboutMe">About Me</a>
      <a class="nav-item nav-link" href="#portfolio">Portfolio</a>
      <a class="btn btn-success nav-item nav-link" href="#contact-bottom">Contact</a>
    </div>
  </nav>

  <!-- LANDING PAGE INTRO-->
  <!-- Each full screen wrapped by section -->
  <section class="landing" id="home">
    <a id="top"></a>
    <div id="overlayDiv">
      <!-- overlay Div used only to show content above canvas element -->
      <div class="container">
        <div class="col-md-12" id="welcome">
          <h2 class="display-5">Hello, I'm</h2>
          <h1 class="display-2"><span id="myName">Marek Jankovič</span></h1>
          <h2 class="display-5">Aspiring full-stack <span class="animatedSpan">web developer.</span></h2>
          <!-- <div class="container" id="buttonMask"> -->
          <div class="row justify-content-center" id="buttonMask">
            <div class="col col-md-2">
              <a href="#aboutMe">
                <div class="btn btn-primary btn-outline-light about-btn">Get to know Me <i class="fa fa-arrow-right"></i></div>
              </a>
            </div>
            <div class="col col-md-2">
              <a href="#contact-bottom">
                <div class="btn btn-success" id="home-contact">Contact Me</div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <canvas id="myCanvas" resize></canvas>
  </section>

  <!-- ABOUT ME -->

  <section id="aboutMe">
    <div class="container-fluid middle">
      <div class="row justify-content-center verticalAlign">
        <div class="col-md-4">
          <div class="about-intro-padding">
            <img class="img-fluid rounded mx-auto" id="profile_img" src="assets/profile-picture-color.png" alt="Marek Jankovic profile photo">
          </div>
        </div>
        <div class="col-md-8" id="about-text">
          <div class="about-intro-padding" id="about-intro-txt">
            <h2 class="animatedSpan">Who am I?</h2>
            <div class="animatedSpan line"></div>
            <p class="text-justify">
            Book-Worm, Dinosaur lover & History Nerd. But primarily an Aspiring Full-Stack Web Developer, who's been really passionate about coding and digital technologies,
             ever since highschool. I've started with C and later on I got into a Python and finally I got hands on HTML, CSS & Javascript.
            </p>
            <p class="text-justify">
              I also spent some time in digital agencies and now I'm here - working for international company as a Lead Web-Editor. Check my LinkedIn page for more details.
            </p>
            <p class="text-justify">
              My newest passion? See below, I finished a Web-Dev bootcamp focused on PHP and I fell in love with it!
            </p>
          </div>
          <div class="about-intro-padding list-margin">
            <div class="row justify-content-center verticalAlign">
              <div class="col-md" id="about-code-txt">
                <h2>I code in</h2>
                <p class="text-justify">Mark-up & programming languages I'm comfortable with</p>
                <ul>
                  <li>
                    <i class="fab fa-html5" aria-hidden="true"></i>
                    <p class="desc-inline">HTML & CSS</p>
                  </li>
                  <li>
                    <i class="fas fa-database"></i>
                    <p class="desc-inline">MySQL</p>
                  </li>
                  <li>
                    <i class="far fa-file-code"></i>
                    <p class="desc-inline">PHP</p>
                  </li>
                </ul>
              </div>

              <div class="col-md" id="about-study-txt">
                <h2>Studying & learning</h2>
                <p class="text-justify">Something I'd love to master</p>
                <ul>
                  <li>
                    <i class="fab fa-js-square"></i>
                    <p class="desc-inline">Javascript & jQuery</p>
                  </li>
                  <li>
                    <i class="fab fa-laravel"></i>
                    <p class="desc-inline">Laravel</p>
                  </li>
                  <li>
                    <i class="fas fa-sync"></i>
                    <p class="desc-inline">Ajax</p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg justify-content-center">
          <p class="sectionEnd">
              <span class="lead">Interested in collab or have a question? Don't hesitate and <a href="#contact" class="animatedSpan">leave me a message!</a></br>
              Or check out my <a href="#portfolio"><span class="animatedSpan">portfolio & sideprojects</span></a> I'm working on.
              </span>
          </p>
        </div>
      </div>
    </div>
  </section>


  <!-- PORTFOLIO -->

  <section id="portfolio">
    <div class="container-fluid middle">
      <div class="row justify-content-center">
        <div class="col-md">
          <!-- h4 is changed based on what carousel item is active -->
          <div id="portfolio_main_description">
            <h4 id="carousel-portfolio">Portfolio</h4>
            <p id="carousel-long-desc"></p>
            <a id="project-button" class="btn btn-success" href="">See More</a>
          </div>
        </div>
      </div>
      <!-- Check and try using Flickity: -->
      <!-- https://css-tricks.com/creating-responsive-touch-friendly-carousels-with-flickity/ -->
      <div class="row justify-content-center">
        <div class="col-md">
          <div class="main-carousel" id="portfolio-carousel">
            <!-- Cycling through array of objects and generating carousel -->
            <?php
if (!empty($portfolio_list)) {

    foreach ($portfolio_list as $item) {

        echo '<div class="carousel-cell" style="background-image: url(' . $item->thumbnail . '";>';
        echo '<div class="portfolio-dim">';
        echo '<h3 class="carousel-name">' . $item->name . '</h3>';
        echo '<span class="portfolio-type">' . $item->type . '</span>';
        echo '</div>';
        echo '<p class="carousel-description">' . $item->description . '</p>';
        // Hidden a tag with href -> using href value in Jquery
        echo '<a class="bcg-img" href="' . $item->background . '"></a>';
        echo '<a class="portfolio-link" href="' . $item->link . '"></a>';
        echo "</div>";
    }
} else {

    echo "<h2>Coming Soon!</h2>";
}
?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- CONTACT page -->
  <section id="contact">
    <div class="container-fluid middle">
      <div class="row justify-content-center">
      <h2 class="animatedSpan">Contact Me</h2>
      </div>
      <div class="row justify-content-center">
        <div class="col-sm-6">
          <form id="main_contactForm">
            <div class="form-group form-row">
              <!-- Name input -->
              <div class="col">
                <label for="form_name" class="label_formName">Your Name</label>
                <input type="text" name="input_formName" id="form_name" class="form-control" placeholder="e.g. Charles Darwin" required>
              </div>
              <!-- Email input -->
              <div class="col">
                <label for="form_email" class="label_formEmail">Your E-mail</label>
                <input type="email" class="input_formEmail form-control" id="form_email" name="input_formEmail" placeholder="e.g. charles@evolution.com" required>
                <small id="emailHelp" class="form-text text-muted">I'll never share your email with anyone else.</small>
              </div>

            </div>
            <div class="form-group form-row">
              <!-- Message input -->
              <div class="col">
                <label for="form_message" class="label_formMessage">Your message</label>
                <textarea name="form_message" class="form-control" id="form_message" name="form_MainMessage" cols="60" rows="10" required></textarea>
              </div>

            </div>
            <button type="submit" name="submit" value="submit" class="btn-lg btn-primary" id="formSubmitButton">Submit</button>
            <div class="alert alert-success" id="formSentMessage" role="alert">Thank you for your message! I'll try to respond as soon as possible :)</div>
          </form>
        </div>
        <div class="col-md-4 social" id="contact-social">
          <div class="row justify-content-center">
            <h4>Find me on social networks</h4>
          </div>

          <div class="row justify-content-center">
            <a href="https://www.linkedin.com/in/marek-jankovic-web-dev/"><i class="fab fa-linkedin"></i></a>
            <a href="https://www.facebook.com/MayoStrange"><i class="fab fa-facebook-square"></i></a>
            <a href="https://www.instagram.com/alistair.strange/"><i class="fab fa-instagram"></i></a>
            <a href="mailto:marek@jankovic.site"><i class="far fa-envelope"></i></a>
          </div>

          <div class="row justify-content-center outro">
            <!-- Coded with <3 by MJ footer -->
            <p class="padding-heart">Coded with </p>
            <svg width="30" height="30" viewBox="0 0 200 200">
              <g transform="translate(100 100)">
                <path transform="translate(-50 -50)" fill="crimson" d="M92.71,7.27L92.71,7.27c-9.71-9.69-25.46-9.69-35.18,0L50,14.79l-7.54-7.52C32.75-2.42,17-2.42,7.29,7.27v0 c-9.71,9.69-9.71,25.41,0,35.1L50,85l42.71-42.63C102.43,32.68,102.43,16.96,92.71,7.27z"></path>
                <animateTransform attributeName="transform" type="scale" values="1; 1.25; 1.45; 1.25; 1;" dur="1.5s" repeatCount="indefinite" additive="sum">
                </animateTransform>
              </g>
            </svg> 

            <?php
              echo '<p class="padding-heart"> by MJ ' . date("Y") . '</p>';
            ?>

             <!-- END OF Coded with <3 by MJ footer -->
          </div>

        </div>
      </div>
    </div>
    <a id="contact-bottom"></a>

      <!-- Sticky BACK TO TOP button -->
    <div class="justify-content-end bd-highlight mb-3" id="back-to-top">
      <a class="btn btn-outline-light nav-item nav-link" href="#home">
      <i class="far fa-arrow-alt-circle-up"></i>
      </a>
    </div>
  </section>

  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script type="text/javascript" src="lib/jquery-3.3.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" src="lib/jquery.cookie.js"></script>
  <!-- Files required for smooth scrolling -->
  <script src='lib/jquery.anchorlink.js'></script>
  <!-- Progressbar JS -->
  <script type="text/javascript" src="lib/progressbar.min.js"></script>
  <!-- Flickity (carousel) JS -->
  <script type="text/javascript" src="js/flickity.pkgd.min.js"></script>
  <!-- Optional JavaScript -->
  <script type="text/javascript" src="js/myScript.min.js"></script>
  <!-- <script type="text/javascript" src="js/anime.min.js"></script> -->
  <script type="text/javascript" src="js/animations.min.js"></script>
  <script type="text/javascript" src="js/scrolling.min.js"></script>

</body>

<footer>
  <a href="privacy-policy.php">Privacy Policy & Cookies</a>
</footer>

</html>