$(document).ready(function(){
	
	// Cookie consent handling
	// Hiding the cookie info if the master cookie has been set
	if($.cookie("master") !== 'undefined' && $.cookie("master") == 'accepted'){
		$('#cookie-consent').hide();
	} else{
		// Height of the cookie div
		var elementHeight = $('#cookie-consent').outerHeight();
		var windowHeight = $(window).outerHeight();

		$('#cookie-consent').css("top", windowHeight - (elementHeight));
	}	

	// AJAX call for setting the master cookie
	$('#cookie-btn').on('click', function(){
		$.ajax({
			type: "POST",
			url: "lib/cookies.php",
			data: "consent-given",
			success: function(data){
				console.log(data);
				$('#cookie-consent').addClass("animated fadeOutDown");
			}
		});
    });
    
    $('#cookie-withdraw').on('click', function(){
		$.ajax({
			type: "POST",
			url: "lib/cookies.php",
			data: "consent-withdrawn",
			success: alert("Cookies have been removed.")
		});
	});

});