<?php
require_once "config.php";
require_once "db.php";
require_once "jankovic_Model.php";

Config::$db = $_conn;

 if(isset($_POST['input_formName']) && isset($_POST['input_formEmail']) && isset($_POST['form_message'])){
        $model = new requestModel();
    
        $model->name = $_POST['input_formName'];
        $model->email = $_POST['input_formEmail'];
        $model->message = $_POST['form_message'];
        $model->sent = 0;

        // TO-DO
        // Actual sending email
        // setting $model->sent to 1 if success else 0

        if(sendEmail($model->name, $model->email, $model->message)){
            $model->sent = 1;
        }

        if($model->insertRequest()){
            return true;
        }
    }


    // Actual sending email
    function sendEmail($senderName, $senderMail, $mailContent){

        // ***RECIPIENT***
        static $recipient = "marek@jankovic.site";

        $headers = "From: no-reply@jankovic.site" . "\r\n" . "Reply-To: " . $senderMail;
        $subject = $senderName . " | jankovic.site Contact Form";

        if(mail($recipient, $subject, $mailContent, $headers)){
            return true;
        }

    }
    
?>

