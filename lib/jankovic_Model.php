<?php

class requestModel{

    // Contact form properties
    public $name;
    public $email;
    public $message;
    public $createdAt;
    public $sent;

    // Contact form methods
    function insertRequest(){
        $this->name = Config::$db->real_escape_string($this->name);
        $this->email = Config::$db->real_escape_string($this->email);
        $this->message = Config::$db->real_escape_string($this->message);

        $sql = "INSERT INTO form_requests (name, email, message, createdAt, sent)
                VALUES ('$this->name', '$this->email', '$this->message', NOW(), '$this->sent')";

        $result = Config::$db->query($sql);

        if($result){
            return true;
        }
    }
}